FROM node
RUN mkdir /somedir
COPY . /somedir
WORKDIR /somedir
RUN yarn install
RUN yarn test
RUN yarn build
